const {ccclass, property} = cc._decorator;

@ccclass
export default class Fight extends cc.Component {
    //original data
    @property
    original_scene:number = 0;
    @property
    enemy_place:string = '';


    //data

    @property
    enemy_type:number = 0;
    @property
    enemy_hp:number = 0;
    @property
    enemy_atk:number = 0;
    @property
    enemy_def:number = 0;
    @property
    enemy_max_hp:number = 0;


    @property
    role_hp:number = 0;
    @property
    role_def:number = 0;
    @property
    role_atk:number = 0;
    @property
    role_max_hp:number = 0;

    @property
    role_revive:number = 0;
    @property
    role_skill_a:number = 0;
    @property
    role_skill_b:number = 0;
    @property
    role_skill_c:number = 0;
    @property
    role_skill_c_flag:number = 0;
    @property
    blood_big_num:number = 0;
    @property
    blood_mid_num:number = 0;
    @property
    blood_small_num:number = 0;
    @property
    cd_num:number = 0;
    @property
    revive_num:number = 0;

    @property
    cd_a_num: number = 0;
    @property
    cd_b_num: number = 0;
    @property
    cd_c_num: number = 0;


    //UI
    
    @property
    round_num:number = 1;
    @property(cc.SpriteFrame)
    role_sprite: cc.Node = null;
    @property(cc.Node)
    container: cc.Node = null;
    @property(cc.Node)
    label:cc.Node = null;
    @property(cc.Node)
    round_label:cc.Node = null;
    @property(cc.Node)
    role_hp_label:cc.Node = null;
    @property(cc.Node)
    en_hp_label:cc.Node= null;
    @property(cc.Node)
    role_hp_rec:cc.Node = null;
    @property(cc.Node)
    en_hp_rec:cc.Node = null;
    @property(cc.Node)
    menu_btn:cc.Node = null;
    @property(cc.Node)
    disable_btn:cc.Node = null;
    @property(cc.Node)
    fight_btn_1:cc.Node = null;
    @property(cc.Node)
    fight_btn_2:cc.Node = null;
    @property(cc.Node)
    fight_btn_3:cc.Node = null;
    @property(cc.Node)
    fight_btn_4:cc.Node = null;

    @property(cc.Node)
    fight_btn_2_cd:cc.Node = null;
    @property(cc.Node)
    fight_btn_3_cd:cc.Node = null;
    @property(cc.Node)
    fight_btn_4_cd:cc.Node = null;
    @property(cc.Node)
    blood_big_btn:cc.Node = null;
    @property(cc.Node)
    blood_mid_btn:cc.Node= null;
    @property(cc.Node)
    blood_small_btn:cc.Node= null;
    @property(cc.Node)
    cd_btn:cc.Node = null;
    @property(cc.Node)
    revive_btn:cc.Node = null;
    @property(cc.Node)
    blood_big_btn_num:cc.Node = null;
    @property(cc.Node)
    blood_mid_btn_num:cc.Node= null;
    @property(cc.Node)
    blood_small_btn_num:cc.Node= null;
    @property(cc.Node)
    cd_btn_num:cc.Node = null;
    @property(cc.Node)
    revive_btn_num:cc.Node = null;
    @property(cc.Node)
    role:cc.Node = null;
    @property(cc.Node)
    attack_an:cc.Node = null;
    @property(cc.Node)
    item_an:cc.Node = null;
    @property(cc.Node)
    enemy:cc.Node = null;
    @property(cc.SpriteFrame)
    en_1:cc.SpriteFrame = null;
    @property(cc.SpriteFrame)
    en_2:cc.SpriteFrame = null;
    @property(cc.SpriteFrame)
    en_3:cc.SpriteFrame = null;
    @property(cc.SpriteFrame)
    en_4:cc.SpriteFrame = null;
    @property(cc.SpriteFrame)
    en_5:cc.SpriteFrame = null;
    @property(cc.Node)
    en_attack_an:cc.Node = null;

    @property(cc.AudioClip)
    normal_battle_bgm: cc.AudioClip = null;

    @property(cc.AudioClip)
    boss_battle_bgm: cc.AudioClip = null;

    @property(cc.AudioClip)
    win_effect: cc.AudioClip = null;

    @property(cc.AudioClip)
    lose_effect: cc.AudioClip = null;

    @property(cc.AudioClip)
    escape_effect: cc.AudioClip = null;

    onLoad(){
        this.download_data();
    }

    start(){
        this.scheduleOnce( ()=>{
            this.set_data();
            this.show();
            this.bgm_play();
        },4);

    }

    update(dt){

    }

    bgm_play(){
        if(this.enemy_type == 5){
            cc.audioEngine.playMusic(this.boss_battle_bgm, true);
        }
        else{
            cc.audioEngine.playMusic(this.normal_battle_bgm, true);
        }
    }

    hind(){
        this.disable_btn.getComponent(cc.Toggle).isChecked = true;
        this.container.x = -550;
    }
    show(){
        this.menu_btn.getComponent(cc.Toggle).isChecked = true;
        this.container.x = -394;
    }
    download_data(){
        var ref1 = firebase.database().ref('nowscene');
        ref1.once('value').then((snap)=>{
            this.original_scene = snap.val().stage;
            cc.log(this.enemy_type);

        });

        var ref2 = firebase.database().ref('man');
        ref2.once('value').then((snap)=>{
            this.role_hp = snap.val().hp;
            this.role_def = snap.val().def;
            this.role_atk = snap.val().atk;
            this.role_revive = snap.val().revive;
            this.role_skill_a = snap.val().skill.a;
            this.role_skill_b = snap.val().skill.b;
            this.role_skill_c = snap.val().skill.c;
            this.blood_big_num = snap.val().bag.bloodsmall;
            this.blood_mid_num = snap.val().bag.bloodmid;
            this.blood_small_num = snap.val().bag.bloodbig;
            this.cd_num = snap.val().bag.cd;
            this.revive_num = snap.val().bag.revive;
            //cc.log('a' + this.role_hp);
        });
        var ref3 = firebase.database().ref('fight');
        ref3.once('value').then((snap)=>{
            this.enemy_type = snap.val().type;
            this.enemy_place = snap.val().place;
            this.enemy_hp = snap.val().testhp;
            this.enemy_def = snap.val().testdef;
            this.enemy_atk = snap.val().testatk;

            cc.log(this.enemy_type);

        });
        this.cd_a_num = 0;
        this.cd_b_num = 0;
        this.cd_c_num = 0;
        this.role_skill_c_flag = 0;

    }
    set_data(){
        if(this.enemy_type == 1){
            this.enemy.getComponent(cc.Sprite).spriteFrame = this.en_1;
            this.enemy_hp = 60;
            this.enemy_atk = 20;
            this.enemy_def = 0; 
        }else if (this.enemy_type == 2){
            this.enemy.getComponent(cc.Sprite).spriteFrame = this.en_2;
            this.enemy_hp = 150;
            this.enemy_atk = 25;
            this.enemy_def = 5; 
        }else if (this.enemy_type == 3){
            this.enemy.getComponent(cc.Sprite).spriteFrame = this.en_3;
            this.enemy_hp = 200;
            this.enemy_atk = 25;
            this.enemy_def = 5; 
        }else if (this.enemy_type == 4){
            this.enemy.getComponent(cc.Sprite).spriteFrame = this.en_4;
            this.enemy_hp = 400;
            this.enemy_atk = 40;
            this.enemy_def = 20; 
        }else if (this.enemy_type == 5){
            this.enemy.getComponent(cc.Sprite).spriteFrame = this.en_5;
            this.enemy_hp = 800;
            this.enemy_atk = 50;
            this.enemy_def = 50; 
        }else{
            this.enemy.getComponent(cc.Sprite).spriteFrame = this.en_1;
        }
        this.role_max_hp = 500;
        this.enemy_max_hp = this.enemy_hp;
        this.attack_an =  this.role.getChildByName('an1');
        this.item_an = this.role.getChildByName('an2');
        this.update_data();

    }

    update_data(){
        //cd num or bag item num
        this.round_label.getComponent(cc.Label).string = String(this.round_num);
        this.update_hp();
        this.fight_btn_2_cd.getComponent(cc.Label).string = String(this.cd_a_num);
        this.fight_btn_3_cd.getComponent(cc.Label).string = String(this.cd_b_num);
        this.fight_btn_4_cd.getComponent(cc.Label).string = String(this.cd_c_num);
        this.blood_big_btn_num.getComponent(cc.Label).string = String(this.blood_big_num);
        this.blood_mid_btn_num.getComponent(cc.Label).string = String(this.blood_mid_num);
        this.blood_small_btn_num.getComponent(cc.Label).string = String(this.blood_small_num);
        this.cd_btn_num.getComponent(cc.Label).string = String(this.cd_num);
        this.revive_btn_num.getComponent(cc.Label).string = String(this.revive_num);
        //skill interactable
        this.fight_btn_2.getComponent(cc.Button).interactable = (this.role_skill_a && (this.cd_a_num == 0))? true : false ;
        this.fight_btn_2.getChildByName('Label').opacity =(this.role_skill_a && (this.cd_a_num == 0))? 255 : 0;
        this.fight_btn_3.getComponent(cc.Button).interactable = (this.role_skill_b && (this.cd_b_num == 0))? true : false ;
        this.fight_btn_3.getChildByName('Label').opacity = (this.role_skill_b && (this.cd_b_num == 0)) ? 255 : 0;
        this.fight_btn_4.getComponent(cc.Button).interactable = (this.role_skill_c && (this.cd_c_num == 0))? true : false ;
        this.fight_btn_4.getChildByName('Label').opacity = (this.role_skill_c && (this.cd_c_num == 0)) ? 255 : 0;
        //cd
        this.fight_btn_2_cd.opacity = (this.role_skill_a && (this.cd_a_num != 0)) ? 255 : 0;
        this.fight_btn_3_cd.opacity = (this.role_skill_b && (this.cd_b_num != 0)) ? 255 : 0;
        this.fight_btn_4_cd.opacity = (this.role_skill_c && (this.cd_c_num != 0)) ? 255 : 0;


        //bag interactable
        this.blood_big_btn.getComponent(cc.Button).interactable = this.blood_big_num ? true : false ;
        this.blood_big_btn.getChildByName('Label').opacity = this.blood_big_num ? 255 : 0;
        this.blood_mid_btn.getComponent(cc.Button).interactable = this.blood_mid_num ? true : false ;
        this.blood_mid_btn.getChildByName('Label').opacity = this.blood_mid_num ? 255 : 0;
        this.blood_small_btn.getComponent(cc.Button).interactable = this.blood_small_num ? true : false ;
        this.blood_small_btn.getChildByName('Label').opacity = this.blood_small_num ? 255 : 0;
        this.cd_btn.getComponent(cc.Button).interactable = this.cd_num ? true : false ;
        this.cd_btn.getChildByName('Label').opacity = this.cd_num ? 255 : 0;
        this.revive_btn.getComponent(cc.Button).interactable = this.revive_num ? true : false ;
        this.revive_btn.getChildByName('Label').opacity = this.revive_num ? 255 : 0;

    }
    update_hp(){
        this.role_hp_label.getComponent(cc.Label).string = String(this.role_hp) + '/' + String(this.role_max_hp);
        this.en_hp_label.getComponent(cc.Label).string = String(this.enemy_hp) + '/' + String(this.enemy_max_hp);
        this.role_hp_rec.width = 300 * (this.role_hp / this.role_max_hp );
        this.en_hp_rec.width = 300 * (this.enemy_hp / this.enemy_max_hp );

    }

    role_action(event, eventdata){
        this.hind();
        cc.log(eventdata);
        var normal_damage =  (this.role_atk > this.enemy_def) ? this.role_atk - this.enemy_def : 0;
        if(eventdata == 1){
            var temp = this.enemy_hp - normal_damage;
            cc.log('skill1');
            this.enemy_hp = (temp > 0)? temp : 0; 
            //skill1
        }else if (eventdata == 2){
            cc.log('slill2');
            var temp = this.enemy_hp - (1.5* normal_damage);
            this.enemy_hp = (temp > 0) ? temp : 0; 
            this.cd_a_num = 3;
            //slill2
        }else if (eventdata == 3){
            var temp = this.enemy_hp - (2.5* normal_damage);
            this.enemy_hp = (temp > 0) ? temp : 0; 
            this.role_hp = (this.role_hp > normal_damage) ? this.role_hp - normal_damage : 0;
            this.cd_b_num = 4;
            cc.log('skill3');
            //skill3
        }else if (eventdata == 4){
            this.cd_c_num = 11;
            this.role_skill_c_flag = 1;
            cc.log('skill4');
            //skill4
        }else if (eventdata == 5){
            var temp = this.role_hp + 100;
            this.role_hp = (temp > 500) ? 500 : temp;
            this.blood_big_num -= 1;
            cc.log('big');
            //blood big
        }else if (eventdata == 6){
            var temp = this.role_hp + 70;
            this.role_hp = (temp > 500) ? 500 : temp;
            this.blood_mid_num -= 1;
            cc.log('mid');
            //blood mid
        }else if (eventdata == 7){
            var temp = this.role_hp + 30;
            this.role_hp = (temp > 500) ? 500 : temp;
            this.blood_small_num -= 1;
            cc.log('small');
            //blood small
        }else if (eventdata == 8){
            this.cd_a_num = 0;
            this.cd_b_num = 0;
            this.cd_c_num = 0;
            this.cd_num -= 1;
            cc.log('cd');
            //cd
        }else if (eventdata == 9){
            this.revive_num -= 1;
            this.role_revive = 1;
            cc.log('revive');
            //revive
        }else if (eventdata == 10){
            this.escape();
            //cc.log('run');
            //run   
            return;
        }
       let dealdata = cc.callFunc(()=>{
           this.scheduleOnce(()=>{
            this.update_hp();
            cc.log('dealdata');

           }, 0.4);
       }, this);
       let attack = cc.callFunc(()=>{
           if(eventdata == 1){
            this.attack_an.getComponent(cc.Animation).play('skill 1');
           }else if (eventdata == 2){
            this.attack_an.getComponent(cc.Animation).play('normal attack');
           }else if (eventdata == 3){
            this.attack_an.getComponent(cc.Animation).play('skill 2');
           }else if (eventdata == 4){
            this.attack_an.getComponent(cc.Animation).play('skill 3');
           }
        }, this);
        let item = cc.callFunc(()=>{
            if((eventdata >= 5) && (eventdata <= 7)){
                this.item_an.y = 8;
             this.item_an.getComponent(cc.Animation).play('HP');
            }else if (eventdata == 8){
                this.item_an.y = 40;
             this.item_an.getComponent(cc.Animation).play('CD');
            }else if (eventdata == 9){
                this.item_an.y = 50;
             this.item_an.getComponent(cc.Animation).play('revive');
            }
         }, this);
       let enemy_time = cc.callFunc(()=>{
           this.scheduleOnce(()=>{
            this.enemy_action();
           },0.5);
        }, this);
        if( (eventdata >= 1) && (eventdata <= 2) ){
            this.role.runAction(cc.sequence(cc.moveBy(1.5, 450, 0) , cc.spawn(attack, cc.moveBy(0.5, 0, 0), dealdata) , cc.moveBy(0.8,  -450, 0), enemy_time));
        }else if (eventdata == 3) {
            this.role.runAction(cc.sequence(cc.moveBy(1.5, 330, 0) , cc.spawn(attack, cc.moveBy(0.5, 0, 0), dealdata) , cc.moveBy(0.8,  -330, 0), enemy_time));
        }else if (eventdata == 4) {
            this.role.runAction(cc.sequence( cc.spawn(attack, cc.moveBy(1, 0, 0), dealdata) , enemy_time));
            //this.role.runAction(cc.sequence(cc.moveBy(1.5, 330, 0) , cc.spawn(attack, cc.moveBy(0.5, 0, 0), dealdata) , cc.moveBy(0.8,  -330, 0), enemy_time));
        }else if ( (eventdata >= 5) && (eventdata <= 9)) {
            this.role.runAction(cc.sequence( cc.spawn(item, cc.moveBy(1, 0, 0), dealdata) , enemy_time));
        }else{
            this.role.runAction(cc.sequence(cc.moveBy(1.5, 0, 100), dealdata, cc.moveBy(0.8,  0, -100), enemy_time));
        }
        //cc.log('123');
        //this.enemy_action();
    }
    enemy_action(){
        cc.log('enemy_time');
        this.cd_a_num = this.cd_a_num == 0 ? 0 : this.cd_a_num-1;
        this.cd_b_num = this.cd_b_num == 0 ? 0 : this.cd_b_num-1;
        this.cd_c_num = this.cd_c_num == 0 ? 0 : this.cd_c_num-1;

        if(this.role_hp > 0 || this.role_revive){
            if(this.role_hp ==0){
                this.role_hp = 500;
                this.update_hp();
            }
            if ( this.enemy_hp != 0)  {
                //enemy atk role
                var normal_damage =  (this.enemy_atk > this.role_def) ? this.enemy_atk - this.role_def : 0;
                if (this.role_skill_c_flag) {
                    var temp = this.enemy_hp - normal_damage;
                    this.enemy_hp = (temp > 0)? temp : 0; 
                    this.role_skill_c_flag = 0;
                    cc.log(this.role_skill_c_flag);
                } else {
                    var temp =  this.role_hp -normal_damage;
                    temp = (temp <= 0 && this.role_skill_c_flag) ? this.role_hp: temp;
                    this.role_skill_c_flag = 0;
                    this.role_hp = temp > 0? temp : 0; 
                }
                let deal_data = cc.callFunc(()=>{
                    this.update_hp();
                    if(this.role_hp  == 0){
                        if(this.role_revive){
                            this.scheduleOnce(()=>{
                                this.role_hp = 500;
                                this.role_revive -= 1;
                                this.update_data();
                            },0.5);
                        }
                    }                    
                    cc.log('dealdata');
               }, this);
               let enemy_time = cc.callFunc(()=>{
                   this.scheduleOnce(()=>{
                    if(this.role_hp == 0){
                        this.die();
                    }else if(this.enemy_hp  == 0){
                        this.win();
                    }else {
                        this.round_num += 1;
                        this.update_data();
                        this.show();
                    }
                   },0.5);
                }, this);
                let en_attack = cc.callFunc(()=>{
                    if(this.enemy_type == 1){
                        this.en_attack_an.x = -300;
                        this.en_attack_an.y = 0;
                     this.en_attack_an.getComponent(cc.Animation).play('slime');
                    }else if (this.enemy_type == 2){
                        this.en_attack_an.x = -300;
                        this.en_attack_an.y = 0;
                     this.en_attack_an.getComponent(cc.Animation).play('wizard');
                    }else if (this.enemy_type == 3){
                        this.en_attack_an.x = -300;
                        this.en_attack_an.y = 0;
                     this.en_attack_an.getComponent(cc.Animation).play('skeleton');
                    }else if (this.enemy_type == 4){
                        this.en_attack_an.x = -300;
                        this.en_attack_an.y = 0;

                     this.en_attack_an.getComponent(cc.Animation).play('knight');
                    }else if (this.enemy_type == 5){
                        this.en_attack_an.x = -320;
                        this.en_attack_an.y = 10;
                        this.en_attack_an.getComponent(cc.Animation).play('boss');
                    }
                 }, this);
                //this.role.runAction(cc.sequence(cc.moveBy(1.5, 450, 0) , cc.spawn(attack, cc.moveBy(0.5, 0, 0), dealdata) , cc.moveBy(0.8,  -450, 0), enemy_time));
                this.enemy.runAction(cc.sequence(cc.moveBy(1.5, -450, 0), cc.spawn(en_attack, cc.moveBy(0.5, 0, 0), deal_data), cc.moveBy(0.8,  450, 0), enemy_time)) ;   
            } else {
                //enemy die
                this.win();
            }
        }else{
            this.die();
        }


    }
    win(){
        cc.log('win');
        cc.audioEngine.stopMusic()
        cc.audioEngine.playEffect(this.win_effect, false);
        this.label.getComponent(cc.Label).string = 'win!';
        this.upload_data(1);
        let action = cc.callFunc(()=>{
            cc.audioEngine.stopAll();
            cc.director.loadScene(String(this.original_scene));
        }, this);
        this.label.runAction(cc.sequence(cc.moveBy(1.5, 0 , 300), cc.moveBy(0.5, 0, 0), action));
        
    }
    die(){
        cc.log('die');
        cc.audioEngine.stopMusic()
        cc.audioEngine.playEffect(this.lose_effect, false);
        this.label.getComponent(cc.Label).string = 'die!';
        this.upload_data(2);
        let action = cc.callFunc(()=>{
            cc.audioEngine.stopAll();
            cc.director.loadScene('Gameover');
        }, this);
        this.label.runAction(cc.sequence(cc.moveBy(1.5, 0 , 300), cc.moveBy(0.5, 0, 0), action));
    }
    escape(){
        cc.log('escape');
        cc.audioEngine.stopMusic()
        cc.audioEngine.playEffect(this.escape_effect, false);
        this.label.getComponent(cc.Label).string = 'run!';
        this.upload_data(3);
        let action = cc.callFunc(()=>{
            cc.audioEngine.stopAll();
            cc.director.loadScene(String(this.original_scene));
        }, this);
        this.label.runAction(cc.sequence(cc.moveBy(1.5, 0 , 300), cc.moveBy(0.5, 0, 0), action));
    }
    upload_data(type : number){

        var ref1 = firebase.database().ref('man');
        ref1.once('value').then(((snap)=>{
            var data = {
                hp : this.role_hp,
                atk: this.role_atk,
                def: this.role_def,
                posX: snap.val().posX,
                posY: snap.val().posY,
                revive: this.role_revive,
            }
            var equipshield = snap.val().bag.equipshield;
            var equipsword = snap.val().bag.equipsword;
            var sword1 = snap.val().bag.sword1;
            var sword2 = snap.val().bag.sword2;
            var shield1 = snap.val().bag.shield1;
            var shield2 = snap.val().bag.shield2;
            var coin = snap.val().bag.coin;
            var key = snap.val().bag.key;
            var exkey = snap.val().bag.exkey;
            var finalkey = snap.val().bag.finalkey;

            ref1.set(data);
            var ref2 = firebase.database().ref('man/skill');
            ref2.once('value').then(((snap)=>{
                var data = {
                    a: this.role_skill_a,
                    b: this.role_skill_b,
                    c: this.role_skill_c,
                }
                ref2.set(data);
            }));
            var ref3 = firebase.database().ref('man/bag');
            var t_coin ;
            if (type == 1) {
                if(this.enemy_type == 1){
                    t_coin = 10;
                }else if (this.enemy_type == 2){
                    t_coin = 20;
                }else if (this.enemy_type == 3){
                    t_coin = 30;
                }else if (this.enemy_type == 4){
                    t_coin = 40;
                }else if (this.enemy_type == 5){
                    t_coin = 50;
                }else{
                    t_coin = 1;

                }
        
            }else{
                t_coin = 0;
            }
            coin += t_coin;
            ref3.once('value').then(((snap)=>{
                var data = {
                    equipsword : equipsword,
                    equipshield: equipshield,
                    bloodsmall: this.blood_big_num,
                    bloodmid: this.blood_mid_num,
                    bloodbig: this.blood_small_num,
                    cd: this.cd_num,
                    revive: this.revive_num,
                    sword1: sword1,
                    sword2: sword2,
                    shield1: shield1,
                    shield2: shield2,
                    coin: coin,
                    key: key,
                    exkey: exkey,
                    finalkey: finalkey,
                }
                ref3.set(data);
            }));
            
        }));

        var ref4 = firebase.database().ref('nowscene');
        ref4.once('value').then(((snap)=>{
            var data = {
                stage : this.original_scene,
            }
            ref4.set(data);
        }));
        
        var ref5 = firebase.database().ref(String(this.original_scene) + '/monster');
        var t;
        if (type == 1) {
            if (this.enemy_type == 0) {
                t = 1;
            }else{
                t = 0;
            }
        }else{
            t = 1;
        }
        ref5.once('value').then(((snap)=>{
            //cc.log(this.enemy_place);
            //cc.log(snap.val().a);
            var data = {
                a: (('a' == this.enemy_place) ? t : snap.val().a)  ,
                b: (('b' == this.enemy_place) ? t :  snap.val().b)  ,
                c: (('c' == this.enemy_place) ? t :  snap.val().c)  ,
                d: (('d' == this.enemy_place) ? t :  snap.val().d)  ,
                e: (('e' == this.enemy_place) ? t :  snap.val().e)  ,
            }
            ref5.set(data);
        }));
        var ref8 = firebase.database().ref('fight');
        ref8.once('value').then(((snap)=>{
            var data = {
                type: this.enemy_type,
                place: this.enemy_place,
                testhp: snap.val().testhp,
                testdef: snap.val().testdef,
                testatk: snap.val().testatk,
                
            }
            ref8.set(data);
        }));

    }
}
