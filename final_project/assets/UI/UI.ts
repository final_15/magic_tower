
const {ccclass, property} = cc._decorator;

@ccclass
export default class UI extends cc.Component {

    @property(cc.Node)
    stage_label: cc.Node = null;
    @property(cc.Node)
    hp_label: cc.Node = null;
    @property(cc.Node)
    atk_label: cc.Node = null;
    @property(cc.Node)
    def_label: cc.Node = null;

    @property(cc.Node)
    key_label: cc.Node = null;


    @property(cc.Node)
    gate: cc.Node = null;
    @property(cc.Node)
    item: cc.Node = null;
    @property(cc.Node)
    enemy:cc.Node = null;


    // LIFE-CYCLE CALLBACKS:

     onLoad () {
         this.stage_label = this.node.getChildByName('stage').getChildByName('stage_num');
         this.hp_label = this.node.getChildByName('hp').getChildByName('hp_num');
         this.atk_label = this.node.getChildByName('atk').getChildByName('atk_num');
         this.def_label = this.node.getChildByName('def').getChildByName('def_num');
         this.key_label = this.node.getChildByName('key').getChildByName('key_num');
         
        this.show_data();
     }

    start () {
        //this.stage_label.getComponent(cc.Label).string = '1';
    }

    show_data(){
        var ref = firebase.database().ref('nowscene');

        ref.once('value').then((snap)=>{
            var stage = snap.val().stage;
            this.stage_label.getComponent(cc.Label).string = String(stage);
            var ref_1 = firebase.database().ref(stage + '/gate');
            var ref_2 = firebase.database().ref(stage + '/item');
            var ref_3 = firebase.database().ref(stage + '/monster');
            ref_1.once('value').then((snap)=>{
                cc.log( 'a ' + snap.val().a);
                cc.log( 'b ' + snap.val().b);
                cc.log( 'c ' + snap.val().c);
                cc.log( 'd ' + snap.val().d);
                cc.log( 'e ' + snap.val().e);

                if(this.gate != null){
                    if(snap.val().a == 0){
                        this.gate.getChildByName('a').destroy();
                    }
                    if(snap.val().b == 0){
                        this.gate.getChildByName('b').destroy();
                    }
                    if(stage != 6){
                        if(snap.val().c == 0){
                            this.gate.getChildByName('c').destroy();
                        }
                        if(snap.val().d == 0){
                            this.gate.getChildByName('d').destroy();
                        }
                        if(snap.val().e== 0){
                            this.gate.getChildByName('e').destroy();
                        }
                    }
                }
            });
            ref_2.once('value').then((snap)=>{
                //cc.log( 'a ' + snap.val().a);
                //cc.log( 'b ' + snap.val().b);
                //cc.log( 'c ' + snap.val().c);
                //cc.log( 'd ' + snap.val().d);
                //cc.log( 'e ' + snap.val().e);
                if(this.item != null){
                    if(snap.val().a == 0){
                        this.item.getChildByName('a').destroy();
                    }
                    if(snap.val().b == 0){
                        //cc.log('destroy');
                        this.item.getChildByName('b').destroy();
                    }
                    if(snap.val().c == 0){
                        this.item.getChildByName('c').destroy();
                    }
                    if(snap.val().d == 0){
                        this.item.getChildByName('d').destroy();
                    }
                    if(snap.val().e== 0){
                        this.item.getChildByName('e').destroy();
                    }
                    if(snap.val().f == 0){
                        this.item.getChildByName('f').destroy();
                    }
                    if(snap.val().g == 0){
                        this.item.getChildByName('g').destroy();
                    }
                    if(snap.val().h == 0){
                        this.item.getChildByName('h').destroy();
                    }
                    if(snap.val().i == 0){
                        this.item.getChildByName('i').destroy();
                    }
                    if(snap.val().j== 0){
                        this.item.getChildByName('j').destroy();
                    }
                }
            });
            ref_3.once('value').then((snap)=>{
                //this.enemy.getChildByName('e').destroy();
                if (this.enemy != null){
                    if(snap.val().a == 0){
                        cc.log('destroy');
                        this.enemy.getChildByName('a').destroy();
                    }
                    if(snap.val().b == 0){
                        this.enemy.getChildByName('b').destroy();
                    }
                    if(snap.val().c == 0){
                        this.enemy.getChildByName('c').destroy();
                    }
                    if(snap.val().d == 0){
                        this.enemy.getChildByName('d').destroy();
                    }
                    if(snap.val().e== 0){
                        cc.log('destroy');
                        this.enemy.getChildByName('e').destroy();
                    }
                }
            });

        });
        this.update_data();
        //this.stage_label.getComponent(cc.Label).string = '2';

    }
     update_data(){
        let the = this;
        var ref1 = firebase.database().ref('man/hp');
        ref1.on('value', function(snapshot) {
            //cc.log( the.hp_label.getComponent(cc.Label).string);
            the.hp_label.getComponent(cc.Label).string = String(snapshot.val());
          });
          /*
        ref1.once('value').then((snap)=>{
            this.hp_label.getComponent(cc.Label).string = String(snap.val());
        });*/
        var ref2 = firebase.database().ref('man/atk');
        ref2.on('value', function(snapshot) {
            the.atk_label.getComponent(cc.Label).string = String(snapshot.val());
          });
        /*
        ref2.once('value').then((snap)=>{
            this.atk_label.getComponent(cc.Label).string = String(snap.val());
        });*/
        var ref3 = firebase.database().ref('man/def');
        ref3.on('value', function(snapshot) {
            the.def_label.getComponent(cc.Label).string = String(snapshot.val());
          });
        /*
        ref3.once('value').then((snap)=>{
            this.def_label.getComponent(cc.Label).string = String(snap.val());
        });*/

        /*
        ref4.once('value').then((snap)=>{
            this.coin_label.getComponent(cc.Label).string = String(snap.val());
        });*/
        var ref5 = firebase.database().ref('man/bag/key');
        ref5.on('value', function(snapshot) {
            the.key_label.getComponent(cc.Label).string = String(snapshot.val());
          });
        /*
        ref5.once('value').then((snap)=>{
            this.key_label.getComponent(cc.Label).string = String(snap.val());
        });*/
     }
}
