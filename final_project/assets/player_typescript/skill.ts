declare var firebase: any;
const {ccclass, property} = cc._decorator;

@ccclass
export default class NewClass extends cc.Component {

    @property(cc.Prefab)
    skill1: cc.Prefab = null;

    @property(cc.Prefab)
    skill2: cc.Prefab = null;
    
    @property(cc.Prefab)
    skill3: cc.Prefab = null;

    // LIFE-CYCLE CALLBACKS:

    async onLoad () {
        let exist_1, exist_2, exist_3: number = 0;
        let database = firebase.database().ref('man');
        await database.once('value').then(function(snapshot){
            snapshot.forEach(function(childShot){
                if(childShot.key == 'skill'){
                    let childData = childShot.val();
                    exist_1 = childData.a;
                    exist_2 = childData.b;
                    exist_3 = childData.c;
                }
            })
        })

        if(exist_1 == 1){
            let sk1 = cc.instantiate(this.skill1);
            this.node.addChild(sk1);
        }
        if(exist_2 == 1){
            let sk2 = cc.instantiate(this.skill2);
            this.node.addChild(sk2);
        }
        if(exist_3 == 1){
            let sk3 = cc.instantiate(this.skill3);
            this.node.addChild(sk3);
        }
    }

    start () {

    }

    // update (dt) {}
}
