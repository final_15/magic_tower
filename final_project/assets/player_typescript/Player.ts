declare var firebase: any;
const { ccclass, property } = cc._decorator;

@ccclass
export class Player extends cc.Component {

    // player move idle frame
    @property(cc.SpriteFrame)
    up_frame: cc.SpriteFrame = null;

    @property(cc.SpriteFrame)
    left_frame: cc.SpriteFrame = null;

    @property(cc.SpriteFrame)
    right_frame: cc.SpriteFrame = null;

    @property(cc.SpriteFrame)
    down_frame: cc.SpriteFrame = null;

    @property(cc.Prefab)
    backpack_window: cc.Prefab = null;

    @property(cc.Prefab)
    skill_window: cc.Prefab = null;

    @property(cc.Prefab)
    equip_window: cc.Prefab = null;

    @property(cc.AudioClip)
    BackgroundMusic:cc.AudioClip = null;

    @property(cc.AudioClip)
    equipment: cc.AudioClip = null;

    @property(cc.AudioClip)
    floor: cc.AudioClip = null;

    @property(cc.AudioClip)
    attack:cc.AudioClip = null;

    @property(cc.AudioClip)
    boss_bgm: cc.AudioClip = null;

    @property(cc.AudioClip)
    princess_bgm: cc.AudioClip = null;

    private anim: cc.Animation = null;

    private playerspeed_x: number = 0;
    private playerspeed_y: number = 0;

    // keydown flag
    up: boolean = false;
    down: boolean = false;
    left: boolean = false;
    right: boolean = false;

    // move dir, 1 = up, 2 = down, 3 = left, 4 = right
    moveDir: number = 1;

    // move flag
    move_cd: boolean = true;

    // start move flag
    isStart: boolean = false;

    // stage flag
    private stage = 0;

    // original pos
    private ori_x = 0;
    private ori_y = 0;
    
    // LIFE-CYCLE CALLBACKS:

    onKeyDown(event) {
        if (event.keyCode == cc.macro.KEY.up) {
            this.up = true;
            this.down = false;
            this.left = false;
            this.right = false;
        }
        else if (event.keyCode == cc.macro.KEY.down) {
            this.up = false;
            this.down = true;
            this.left = false;
            this.right = false;
        }
        else if (event.keyCode == cc.macro.KEY.left) {
            this.up = false;
            this.down = false;
            this.left = true;
            this.right = false;
        }
        else if (event.keyCode == cc.macro.KEY.right) {
            this.up = false;
            this.down = false;
            this.left = false;
            this.right = true;
        }
    }

    onKeyUp(event) {
        if (event.keyCode == cc.macro.KEY.up) {
            this.up = false;
        }
        else if (event.keyCode == cc.macro.KEY.down) {
            this.down = false;
        }
        else if (event.keyCode == cc.macro.KEY.left) {
            this.left = false;
        }
        else if (event.keyCode == cc.macro.KEY.right) {
            this.right = false;
        }
    }

    backpack_button() {
        if (cc.isValid(cc.find('Canvas/skill'))) {
            return;
        }
        else if (cc.isValid(cc.find('Canvas/equipment'))) {
            return;
        }
        else {
            if (cc.isValid(cc.find('Canvas/backpack'))) {
                cc.find('Canvas/backpack').destroy();
                this.isStart = true;
            }
            else {
                let backpack_window = cc.instantiate(this.backpack_window);
                cc.find('Canvas').addChild(backpack_window);
                this.isStart = false;
            }
        }
    }

    skill_button() {
        if (cc.isValid(cc.find('Canvas/backpack'))) {
            return;
        }
        else if (cc.isValid(cc.find('Canvas/equipment'))) {
            return;
        }
        else {
            if (cc.isValid(cc.find('Canvas/skill'))) {
                cc.find('Canvas/skill').destroy();
                this.isStart = true;
            }
            else {
                let skill_window = cc.instantiate(this.skill_window);
                cc.find('Canvas').addChild(skill_window);
                this.isStart = false;
            }
        }
    }

    equip_button() {
        if (cc.isValid(cc.find('Canvas/skill'))) {
            return;
        }
        else if (cc.isValid(cc.find('Canvas/backpack'))) {
            return;
        }
        else {
            if (cc.isValid(cc.find('Canvas/equipment'))) {
                cc.find('Canvas/equipment').destroy();
                this.isStart = true;
            }
            else {
                let equip_window = cc.instantiate(this.equip_window);
                cc.find('Canvas').addChild(equip_window);
                this.isStart = false;
            }
        }
    }

    async onLoad() {
        cc.director.getPhysicsManager().enabled = true;
        this.anim = this.getComponent(cc.Animation);
        cc.systemEvent.on(cc.SystemEvent.EventType.KEY_DOWN, this.onKeyDown, this);
        cc.systemEvent.on(cc.SystemEvent.EventType.KEY_UP, this.onKeyUp, this);
        var collide_manager = cc.director.getCollisionManager();
        collide_manager.enabled = true;

        this.scheduleOnce(function(){
            this.isStart = true;
            if(this.stage == 5){
                cc.audioEngine.playMusic(this.boss_bgm, true);
            }
            else if(this.stage == 6){
                cc.audioEngine.playMusic(this.princess_bgm, true);
            }
            else{
                cc.audioEngine.playMusic(this.BackgroundMusic, true);
            }
        }, 4)

        let button_back = new cc.Component.EventHandler();
        button_back.target = this.node;
        button_back.component = "Player";
        button_back.handler = "backpack_button";
        cc.find('Canvas/UI/Backage').getComponent(cc.Button).clickEvents.push(button_back);

        let button_skill = new cc.Component.EventHandler();
        button_skill.target = this.node;
        button_skill.component = "Player";
        button_skill.handler = "skill_button";
        cc.find('Canvas/UI/skill').getComponent(cc.Button).clickEvents.push(button_skill);

        let button_equip = new cc.Component.EventHandler();
        button_equip.target = this.node;
        button_equip.component = "Player";
        button_equip.handler = "equip_button";
        cc.find('Canvas/UI/equipment').getComponent(cc.Button).clickEvents.push(button_equip);

        var ref = firebase.database().ref('nowscene');
        await ref.once('value').then((snap)=>{
            this.stage = snap.val().stage
        });
        

        let init_x, init_y = 0;
        let enemy_pos = 0;
        let prev_stage = '';
        var ref = firebase.database().ref(this.stage.toString() + '/pos')
        await ref.once('value').then((snap)=>{
            init_x = snap.val().x;
            init_y = snap.val().y;
            prev_stage = snap.val().prev_stage;
            enemy_pos = snap.val().enemy_pos;
        });
        if(prev_stage == 'fight'){
            if(enemy_pos == 1){
                this.node.setPosition(cc.v2(init_x - 4.05, init_y));
            }
            else if(enemy_pos == 2){
                this.node.setPosition(cc.v2(init_x + 4.05, init_y));
            }
            else if(enemy_pos == 3){
                this.node.setPosition(cc.v2(init_x, init_y - 4.05));
            }
            else if(enemy_pos == 4){
                this.node.setPosition(cc.v2(init_x, init_y + 4.05));
            }
        }
        else{
            this.node.setPosition(cc.v2(init_x, init_y));
            cc.log(this.node.x, this.node.y)
        }
        if(prev_stage != 'fight'){
            this.ori_x = init_x;
            this.ori_y = init_y
        }
    }

    start() {

    }

    playerMovement(dt) {
        this.playerspeed_x = 0;
        this.playerspeed_y = 0;
        if (this.up) {
            if (this.move_cd) {
                this.move_cd = false;
                this.scheduleOnce(function () {
                    this.move_cd = true;
                }, 0.1);
                this.playerspeed_x = 0;
                this.playerspeed_y = 30;
            }
            else {
                this.playerspeed_x = 0;
                this.playerspeed_y = 0;
            }
        }
        else if (this.down) {
            if (this.move_cd) {
                this.move_cd = false;
                this.scheduleOnce(function () {
                    this.move_cd = true;
                }, 0.1);
                this.playerspeed_x = 0;
                this.playerspeed_y = -30;
            }
            else {
                this.playerspeed_x = 0;
                this.playerspeed_y = 0;
            }
        }
        else if (this.right) {
            if (this.move_cd) {
                this.move_cd = false;
                this.scheduleOnce(function () {
                    this.move_cd = true;
                }, 0.1);
                this.playerspeed_x = 30;
                this.playerspeed_y = 0;
            }
            else {
                this.playerspeed_x = 0;
                this.playerspeed_y = 0;
            }
        }
        else if (this.left) {
            if (this.move_cd) {
                this.move_cd = false;
                this.scheduleOnce(function () {
                    this.move_cd = true;
                }, 0.1);
                this.playerspeed_x = -30;
                this.playerspeed_y = 0;
            }
            else {
                this.playerspeed_x = 0;
                this.playerspeed_y = 0;
            }
        }
        this.node.getComponent(cc.RigidBody).linearVelocity = cc.v2(this.playerspeed_x * 64, this.playerspeed_y * 64);

    }

    playerAnimation() {
        if (this.up) {
            this.node.getComponent(cc.Sprite).spriteFrame = this.up_frame;
            if (!this.anim.getAnimationState('walk_up').isPlaying) {
                this.anim.play('walk_up');
            }
            this.moveDir = 1;
        }
        else if (this.down) {
            this.node.getComponent(cc.Sprite).spriteFrame = this.down_frame;
            if (!this.anim.getAnimationState('walk_down').isPlaying) {
                this.anim.play('walk_down');
            }
            this.moveDir = 2;
        }
        else if (this.left) {
            this.node.getComponent(cc.Sprite).spriteFrame = this.left_frame;
            if (!this.anim.getAnimationState('walk_left').isPlaying) {
                this.anim.play('walk_left');
            }
            this.moveDir = 3;
        }
        else if (this.right) {
            this.node.getComponent(cc.Sprite).spriteFrame = this.right_frame;
            if (!this.anim.getAnimationState('walk_right').isPlaying) {
                this.anim.play('walk_right');
            }
            this.moveDir = 4;
        }
        else {
            this.anim.stop();
            if (this.moveDir == 1) {
                this.node.getComponent(cc.Sprite).spriteFrame = this.up_frame;
            }
            else if (this.moveDir == 2) {
                this.node.getComponent(cc.Sprite).spriteFrame = this.down_frame;
            }
            else if (this.moveDir == 3) {
                this.node.getComponent(cc.Sprite).spriteFrame = this.left_frame;
            }
            else if (this.moveDir == 4) {
                this.node.getComponent(cc.Sprite).spriteFrame = this.right_frame;
            }
        }
    }

    posX_posY(){
        let database = firebase.database().ref(this.stage.toString() + '/pos');
        database.update({
            x: this.node.x,
            y: this.node.y
        })
    }

    update(dt) {
        if(this.isStart){
            this.posX_posY();
            this.playerMovement(dt);
            this.playerAnimation();
        }
        cc.log(this.node.x, this.node.y)
    }

    onCollisionEnter(other, self){
        let database = firebase.database().ref('man/bag');
        // normal_sword
        if(other.tag == 12){
            cc.audioEngine.playEffect(this.equipment, false);
            database.update({
                sword1: 1
            })
            let data = firebase.database().ref(this.stage.toString() + '/item/' + other.node.name);
            data.set(0)
        }
        // ex_sword
        else if(other.tag == 13){
            cc.audioEngine.playEffect(this.equipment, false);
            database.update({
                sword2: 1
            })
            let data = firebase.database().ref(this.stage.toString() + '/item/' + other.node.name);
            data.set(0)
        }
        // normal_shield
        else if(other.tag == 14){
            cc.audioEngine.playEffect(this.equipment, false);
            database.update({
                shield1: 1
            })
            let data = firebase.database().ref(this.stage.toString() + '/item/' + other.node.name);
            data.set(0)
        }
        // ex_shield
        else if(other.tag == 15){
            cc.audioEngine.playEffect(this.equipment, false);
            database.update({
                shield2: 1
            })
            let data = firebase.database().ref(this.stage.toString() + '/item/' + other.node.name);
            data.set(0)
        }
        // slime
        else if(other.tag == 22){
            cc.audioEngine.playEffect(this.attack, false);
            cc.audioEngine.stopMusic();
            let enemy_pos = 0;
            // 1 for right, 2 for left, 3 for up, 4 for down
            if(other.node.x - this.node.x > 10){
                enemy_pos = 1
            }
            else if(other.node.x - this.node.x < -10){
                enemy_pos = 2
            }
            else if(other.node.y - this.node.y > 10){
                enemy_pos = 3;
            }
            else if(other.node.y - this.node.y < -10){
                enemy_pos = 4;
            }
            if((other.node.x - this.node.x) > 10){
                this.node.x -= 4.05;
            }
            else if((other.node.x - this.node.x)  < -10){
                this.node.x += 4.05;
            }
            else if((other.node.y - this.node.y)  > 10){
                this.node.y -= 4.05;
            }
            else if((other.node.y - this.node.y)  < -10){
                this.node.y += 4.05;
            }
            let database = firebase.database().ref(this.stage.toString() + '/pos');
            database.update({
                prev_stage: 'fight',
                enemy_pos: enemy_pos
            })
        }
        // skeleton
        else if(other.tag == 23){
            cc.audioEngine.playEffect(this.attack, false);
            cc.audioEngine.stopMusic();
            let enemy_pos = 0;
            // 1 for right, 2 for left, 3 for up, 4 for down
            if(other.node.x - this.node.x > 10){
                enemy_pos = 1
            }
            else if(other.node.x - this.node.x < -10){
                enemy_pos = 2
            }
            else if(other.node.y - this.node.y > 10){
                enemy_pos = 3;
            }
            else if(other.node.y - this.node.y < -10){
                enemy_pos = 4;
            }
            if((other.node.x - this.node.x) > 10){
                this.node.x -= 4.05;
            }
            else if((other.node.x - this.node.x)  < -10){
                this.node.x += 4.05;
            }
            else if((other.node.y - this.node.y)  > 10){
                this.node.y -= 4.05;
            }
            else if((other.node.y - this.node.y)  < -10){
                this.node.y += 4.05;
            }
            let database = firebase.database().ref(this.stage.toString() + '/pos');
            database.update({
                prev_stage: 'fight',
                enemy_pos: enemy_pos
            })
        }
        // wizard
        else if(other.tag == 24){
            cc.audioEngine.playEffect(this.attack, false);
            cc.audioEngine.stopMusic();
            let enemy_pos = 0;
            // 1 for right, 2 for left, 3 for up, 4 for down
            if(other.node.x - this.node.x > 10){
                enemy_pos = 1
            }
            else if(other.node.x - this.node.x < -10){
                enemy_pos = 2
            }
            else if(other.node.y - this.node.y > 10){
                enemy_pos = 3;
            }
            else if(other.node.y - this.node.y < -10){
                enemy_pos = 4;
            }
            if((other.node.x - this.node.x) > 10){
                this.node.x -= 4.05;
            }
            else if((other.node.x - this.node.x)  < -10){
                this.node.x += 4.05;
            }
            else if((other.node.y - this.node.y)  > 10){
                this.node.y -= 4.05;
            }
            else if((other.node.y - this.node.y)  < -10){
                this.node.y += 4.05;
            }
            let database = firebase.database().ref(this.stage.toString() + '/pos');
            database.update({
                prev_stage: 'fight',
                enemy_pos: enemy_pos
            })
        }
        // knight
        else if(other.tag == 25){
            cc.audioEngine.playEffect(this.attack, false);
            cc.audioEngine.stopMusic();
            let enemy_pos = 0;
            // 1 for right, 2 for left, 3 for up, 4 for down
            if(other.node.x - this.node.x > 10){
                enemy_pos = 1
            }
            else if(other.node.x - this.node.x < -10){
                enemy_pos = 2
            }
            else if(other.node.y - this.node.y > 10){
                enemy_pos = 3;
            }
            else if(other.node.y - this.node.y < -10){
                enemy_pos = 4;
            }
            if((other.node.x - this.node.x) > 10){
                this.node.x -= 4.05;
            }
            else if((other.node.x - this.node.x)  < -10){
                this.node.x += 4.05;
            }
            else if((other.node.y - this.node.y)  > 10){
                this.node.y -= 4.05;
            }
            else if((other.node.y - this.node.y)  < -10){
                this.node.y += 4.05;
            }
            let database = firebase.database().ref(this.stage.toString() + '/pos');
            database.update({
                prev_stage: 'fight',
                enemy_pos: enemy_pos
            })
        }
        // boss
        else if(other.tag == 26){
            cc.audioEngine.playEffect(this.attack, false);
            cc.audioEngine.stopMusic();
            let enemy_pos = 0;
            // 1 for right, 2 for left, 3 for up, 4 for down
            if(other.node.x - this.node.x > 10){
                enemy_pos = 1
            }
            else if(other.node.x - this.node.x < -10){
                enemy_pos = 2
            }
            else if(other.node.y - this.node.y > 10){
                enemy_pos = 3;
            }
            else if(other.node.y - this.node.y < -10){
                enemy_pos = 4;
            }
            if((other.node.x - this.node.x) > 10){
                this.node.x -= 4.05;
            }
            else if((other.node.x - this.node.x)  < -10){
                this.node.x += 4.05;
            }
            else if((other.node.y - this.node.y)  > 10){
                this.node.y -= 4.05;
            }
            else if((other.node.y - this.node.y)  < -10){
                this.node.y += 4.05;
            }
            let database = firebase.database().ref(this.stage.toString() + '/pos');
            database.update({
                prev_stage: 'fight',
                enemy_pos: enemy_pos
            })
        }
        // upstair
        else if(other.tag == 31){
            cc.audioEngine.playEffect(this.floor, false);
            cc.audioEngine.stopMusic();
            this.isStart = false;
            let data = firebase.database().ref('nowscene')
            data.update({
                stage: this.stage + 1
            })
            if(this.stage == 1){
                data = firebase.database().ref((this.stage).toString() + '/pos');
                data.update({
                    x: this.node.x - 32,
                    y: this.node.y
                })
            }
            else if(this.stage == 2){
                data = firebase.database().ref((this.stage).toString() + '/pos');
                data.update({
                    x: this.node.x,
                    y: this.node.y - 32
                })
            }
            else if(this.stage == 3){
                data = firebase.database().ref((this.stage).toString() + '/pos');
                data.update({
                    x: this.node.x,
                    y: this.node.y + 32
                })
            }
            else if(this.stage == 4){
                data = firebase.database().ref((this.stage).toString() + '/pos');
                data.update({
                    x: this.node.x,
                    y: this.node.y - 32
                })
            }
            else if(this.stage == 5){
                data = firebase.database().ref((this.stage).toString() + '/pos');
                data.update({
                    x: this.node.x,
                    y: this.node.y - 32
                })
            }
            data = firebase.database().ref((this.stage+1).toString() + '/pos');
            data.update({
                prev_stage: this.stage,
                enemy_pos: 0
            })
            cc.director.loadScene((this.stage + 1).toString());
        }
        // downstair
        else if(other.tag == 32){
            cc.audioEngine.playEffect(this.floor, false);
            cc.audioEngine.stopMusic();
            this.isStart = false;
            let data = firebase.database().ref('nowscene')
            data.update({
                stage: this.stage - 1
            })
            if(this.stage == 2){
                data = firebase.database().ref((this.stage).toString() + '/pos');
                data.update({
                    x: this.node.x + 32,
                    y: this.node.y
                })
            }
            else if(this.stage == 3){
                data = firebase.database().ref((this.stage).toString() + '/pos');
                data.update({
                    x: this.node.x - 32,
                    y: this.node.y
                })
            }
            else if(this.stage == 4){
                data = firebase.database().ref((this.stage).toString() + '/pos');
                data.update({
                    x: this.node.x + 32,
                    y: this.node.y
                })
            }
            else if(this.stage == 5){
                data = firebase.database().ref((this.stage).toString() + '/pos');
                data.update({
                    x: this.node.x,
                    y: this.node.y + 32
                })
            }
            else if(this.stage == 6){
                data = firebase.database().ref((this.stage).toString() + '/pos');
                data.update({
                    x: this.node.x + 32,
                    y: this.node.y
                })
            }
            data = firebase.database().ref((this.stage - 1).toString() + '/pos');
            data.update({
                prev_stage: this.stage,
                enemy_pos: 0
            })
            cc.director.loadScene((this.stage - 1).toString());
        }
        // hp small
        else if(other.tag == 2){
            cc.audioEngine.playEffect(this.equipment, false);
            let data = firebase.database().ref(this.stage.toString() + '/item/' + other.node.name);
            data.set(0)
        }
        // hp medium
        else if(other.tag == 3){
            cc.audioEngine.playEffect(this.equipment, false);
            let data = firebase.database().ref(this.stage.toString() + '/item/' + other.node.name);
            data.set(0)
        }
        // hp large
        else if(other.tag == 4){
            cc.audioEngine.playEffect(this.equipment, false);
            let data = firebase.database().ref(this.stage.toString() + '/item/' + other.node.name);
            data.set(0)
        }
        // CD
        else if(other.tag == 5){
            cc.audioEngine.playEffect(this.equipment, false);
            let data = firebase.database().ref(this.stage.toString() + '/item/' + other.node.name);
            data.set(0)
        }
        // revive
        else if(other.tag == 6){
            cc.audioEngine.playEffect(this.equipment, false);
            let data = firebase.database().ref(this.stage.toString() + '/item/' + other.node.name);
            data.set(0)
        }
        // attack crystal
        else if(other.tag == 7){
            cc.audioEngine.playEffect(this.equipment, false);
            let data = firebase.database().ref(this.stage.toString() + '/item/' + other.node.name);
            data.set(0)
        }
        // def crystal
        else if(other.tag == 8){
            cc.audioEngine.playEffect(this.equipment, false);
            let data = firebase.database().ref(this.stage.toString() + '/item/' + other.node.name);
            data.set(0)
        }
        // n_key
        else if(other.tag == 9){
            cc.audioEngine.playEffect(this.equipment, false);
            let data = firebase.database().ref(this.stage.toString() + '/item/' + other.node.name);
            data.set(0)
        }
        // ex_key
        else if(other.tag == 10){
            cc.audioEngine.playEffect(this.equipment, false);
            let data = firebase.database().ref(this.stage.toString() + '/item/' + other.node.name);
            data.set(0)
        }
        // f_key
        else if(other.tag == 11){
            cc.audioEngine.playEffect(this.equipment, false);
            let data = firebase.database().ref(this.stage.toString() + '/item/' + other.node.name);
            data.set(0)
        }
        // skill
        else if(other.tag == 16){
            cc.audioEngine.playEffect(this.equipment, false);
            let data = firebase.database().ref(this.stage.toString() + '/item/' + other.node.name);
            data.set(0)
        }
        else if(other.tag == 17){
            cc.audioEngine.playEffect(this.equipment, false);
            let data = firebase.database().ref(this.stage.toString() + '/item/' + other.node.name);
            data.set(0)
        }
        else if(other.tag == 18){
            cc.audioEngine.playEffect(this.equipment, false);
            let data = firebase.database().ref(this.stage.toString() + '/item/' + other.node.name);
            data.set(0)
        }
        else if(other.tag == 19){
            cc.log('test')
            if(other.node.x - this.node.x > 10){
                this.node.x -= 1.05;
            }
            else if(other.node.x - this.node.x < -10){
                this.node.x += 1.05;
            }
            else if(other.node.y - this.node.y > 10){
                this.node.y -= 1.05;
            }
            else if(other.node.y - this.node.y < -10){
                this.node.y += 1.05;
            }
        }
        else if(other.tag == 20){
            if(other.node.x - this.node.x > 10){
                this.node.x -= 1.05;
            }
            else if(other.node.x - this.node.x < -10){
                this.node.x += 1.05;
            }
            else if(other.node.y - this.node.y > 10){
                this.node.y -= 1.05;
            }
            else if(other.node.y - this.node.y < -10){
                this.node.y += 1.05;
            }
        }
        else if(other.tag == 21){
            if(other.node.x - this.node.x > 10){
                this.node.x -= 1.05;
            }
            else if(other.node.x - this.node.x < -10){
                this.node.x += 1.05;
            }
            else if(other.node.y - this.node.y > 10){
                this.node.y -= 1.05;
            }
            else if(other.node.y - this.node.y < -10){
                this.node.y += 1.05;
            }
        }
    }
    onBeginContact(contact, self, other){
        if(other.node.name == 'Real_princess'){
            cc.audioEngine.stopMusic();
            cc.director.loadScene('goodclear');
        }
        else if(other.node.name == 'Bad_princess'){
            
            cc.audioEngine.stopMusic();
            cc.director.loadScene('badclear');
        }
    }
}
