declare var firebase: any;
const {ccclass, property} = cc._decorator;

@ccclass
export default class NewClass extends cc.Component {

    private hp_small_num = 0;

    private hp_medium_num = 0;

    private hp_large_num = 0;

    private cd_potion = 0;

    private revive = 0;

    private EX_key = 0;

    private Final_key = 0;

    // LIFE-CYCLE CALLBACKS:

    async onLoad () {
        let hp_s = 0;
        let hp_m = 0;
        let hp_l = 0;
        let cd = 0;
        let re = 0;
        let ek = 0;
        let fk = 0;
        let ref = firebase.database().ref('man');
        await ref.once('value').then(function(snapshot){
            snapshot.forEach(function(childShot){
                if(childShot.key == 'bag'){
                    let childData = childShot.val();
                    hp_s = childData.bloodsmall;
                    hp_m = childData.bloodmid;
                    hp_l = childData.bloodbig;
                    cd = childData.cd;
                    re = childData.revive;
                    ek = childData.exkey;
                    fk = childData.finalkey;
                }
            })
        })

        this.hp_large_num = hp_l;
        this.hp_medium_num = hp_m;
        this.hp_small_num = hp_s;
        this.cd_potion = cd;
        this.revive = re;
        this.EX_key = ek;
        this.Final_key = fk;

        cc.find('Canvas/backpack/bloodsmall/Background/num').getComponent(cc.Label).string = this.hp_small_num.toString();
        cc.find('Canvas/backpack/bloodmedium/Background/num').getComponent(cc.Label).string = this.hp_medium_num.toString();
        cc.find('Canvas/backpack/bloodlarge/Background/num').getComponent(cc.Label).string = this.hp_large_num.toString();
        cc.find('Canvas/backpack/cd_potion/Background/num').getComponent(cc.Label).string = this.cd_potion.toString();
        cc.find('Canvas/backpack/revive/Background/num').getComponent(cc.Label).string = this.revive.toString();
        cc.find('Canvas/backpack/EX_key/Background/num').getComponent(cc.Label).string = this.EX_key.toString();
        cc.find('Canvas/backpack/Final_key/Background/num').getComponent(cc.Label).string = this.Final_key.toString();
    }

    start () {

    }

    // update (dt) {}
}
