declare var firebase: any;
const {ccclass, property} = cc._decorator;

@ccclass
export default class NewClass extends cc.Component {

    @property(cc.SpriteFrame)
    df_btn_frame: cc.SpriteFrame = null;

    @property(cc.Node)
    weapon_background: cc.Node = null;

    @property(cc.Node)
    shield_background: cc.Node = null;

    @property(cc.SpriteFrame)
    normal_sword_frame: cc.SpriteFrame = null;

    @property(cc.SpriteFrame)
    normal_shield_frame: cc.SpriteFrame = null;

    @property(cc.SpriteFrame)
    ex_sword_frame: cc.SpriteFrame = null;

    @property(cc.SpriteFrame)
    ex_shield_frame: cc.SpriteFrame = null;

    @property(cc.Prefab)
    normal_sword_prefab: cc.Prefab = null;

    @property(cc.Prefab)
    normal_shield_prefab: cc.Prefab = null;

    @property(cc.Prefab)
    ex_sword_prefab: cc.Prefab = null;

    @property(cc.Prefab)
    ex_shield_prefab: cc.Prefab = null;

    // flag to check if pick up equipment, 0 = not, 1 = have, 2 = equip
    public normal_sword_get: number = 0;

    public normal_shield_get: number = 0;

    public ex_sword_get: number = 0;

    public ex_shield_get: number = 0;

    // 0 = wear nothing, 1 = normal, 2 = ex
    public sword_wear: number = 0;

    public shield_wear: number = 0;

    // equip button pos
    private equip_button_x = -200;
    private equip_button_y = -120;

    // now have how many equip
    // 0 = none, 1 = normal, 2 = ex;
    private weapon_equip = 0;
    private shield_equip = 0;
    
    // which choose
    private sw1_click: boolean = false;
    private sh1_click: boolean = false;
    private sw2_click: boolean = false;
    private sh2_click: boolean = false;

    // which choose for upper weapon and shield
    private up_weapon_click: boolean = false;
    private up_shield_click: boolean = false;

    // LIFE-CYCLE CALLBACKS:

    up_weapon_btn(){
        if(this.sword_wear == 1){
            if(this.up_weapon_click){
                this.sword_wear = 0;
                let database = firebase.database().ref('man/bag');
                database.update({
                    equipsword: 0, 
                });
                let atk_num = cc.find('Canvas/UI/atk/atk_num').getComponent(cc.Label).string;
                let atknum;
                atknum = parseInt(atk_num);
                database = firebase.database().ref('man');
                database.update({
                    atk: atknum - 20
                })
                this.up_weapon_click = false;
                this.up_shield_click = false;
            }
            else{
                this.up_weapon_click = true;
                this.up_shield_click = false;
            }
        }
        else if(this.sword_wear == 2){
            if(this.up_weapon_click){
                this.sword_wear = 0;
                let database = firebase.database().ref('man/bag');
                database.update({
                    equipsword: 0, 
                });
                let atk_num = cc.find('Canvas/UI/atk/atk_num').getComponent(cc.Label).string;
                let atknum;
                atknum = parseInt(atk_num);
                database = firebase.database().ref('man');
                database.update({
                    atk: atknum - 50
                })
                this.up_weapon_click = false;
                this.up_shield_click = false;
            }
            else{
                this.up_weapon_click = true;
                this.up_shield_click = false;
            }
        }
    }

    up_shield_btn(){
        if(this.shield_wear == 1){
            if(this.up_shield_click){
                this.shield_wear = 0;
                let database = firebase.database().ref('man/bag');
                database.update({
                    equipshield: 0, 
                });
                let def_num = cc.find('Canvas/UI/def/def_num').getComponent(cc.Label).string;
                let defnum;
                defnum = parseInt(def_num);
                database = firebase.database().ref('man');
                database.update({
                    def: defnum - 10
                })
                this.up_shield_click = false;
                this.up_weapon_click = false;
            }
            else{
                this.up_shield_click = true;
                this.up_weapon_click = false;
            }
        }
        else if(this.shield_wear == 2){
            if(this.up_shield_click){
                this.shield_wear = 0;
                let database = firebase.database().ref('man/bag');
                database.update({
                    equipshield: 0, 
                });
                let def_num = cc.find('Canvas/UI/def/def_num').getComponent(cc.Label).string;
                let defnum;
                defnum = parseInt(def_num);
                database = firebase.database().ref('man');
                database.update({
                    def: defnum - 15
                })
                this.up_shield_click = false;
                this.up_weapon_click = false;
            }
            else{
                this.up_shield_click = true;
                this.up_weapon_click = false;
            }
        }
    }

    sword1_btn(){
        if(this.sword_wear == 0){
            if(this.sw1_click){
                cc.find('Canvas/equipment/normal_weapon_btn/choose').getComponent(cc.Label).string = "";
                let database = firebase.database().ref('man/bag');
                database.update({
                    equipsword: 1, 
                });
                let atk_num = cc.find('Canvas/UI/atk/atk_num').getComponent(cc.Label).string;
                let atknum;
                atknum = parseInt(atk_num);
                cc.log(atk_num);
                database = firebase.database().ref('man');
                database.update({
                    atk: atknum + 20
                })
                this.sword_wear = 1;
                this.sw1_click = false;
            }
            else if(this.sh1_click){
                cc.find('Canvas/equipment/normal_shield_btn/choose').getComponent(cc.Label).string = "";
                this.sh1_click = false;
            }
            else if(this.sw2_click){
                cc.find('Canvas/equipment/ex_sword_btn/choose').getComponent(cc.Label).string = "";
                this.sw2_click = false;
            }
            else if(this.sh2_click){
                cc.find('Canvas/equipment/ex_shield_btn/choose').getComponent(cc.Label).string = "";
                this.sh2_click = false;
            }
            if(!this.sw1_click && this.sword_wear == 0){
                this.sw1_click = true;
                cc.find('Canvas/equipment/normal_weapon_btn/choose').getComponent(cc.Label).string = '\u9078\u53d6'; 
            }
        }
        /*else  if(this.sword_wear == 1){

        }*/
        else if(this.sword_wear == 2){
            if(this.sw1_click){
                cc.find('Canvas/equipment/normal_weapon_btn/choose').getComponent(cc.Label).string = "";
                let database = firebase.database().ref('man/bag');
                database.update({
                    equipsword: 1, 
                });
                let atk_num = cc.find('Canvas/UI/atk/atk_num').getComponent(cc.Label).string;
                let atknum;
                atknum = parseInt(atk_num);
                cc.log(atk_num);
                database = firebase.database().ref('man');
                database.update({
                    atk: atknum - 30
                })
                this.sword_wear = 1;
                this.sw1_click = false;
            }
            else if(this.sh1_click){
                cc.find('Canvas/equipment/normal_shield_btn/choose').getComponent(cc.Label).string = "";
                this.sh1_click = false;
            }
            else if(this.sw2_click){
                cc.find('Canvas/equipment/ex_sword_btn/choose').getComponent(cc.Label).string = "";
                this.sw2_click = false;
            }
            else if(this.sh2_click){
                cc.find('Canvas/equipment/ex_shield_btn/choose').getComponent(cc.Label).string = "";
                this.sh2_click = false;
            }
            if(!this.sw1_click && this.sword_wear == 2){
                this.sw1_click = true;
                cc.find('Canvas/equipment/normal_weapon_btn/choose').getComponent(cc.Label).string = '\u9078\u53d6'; 
            }
        }
    }

    shield1_btn(){
        if(this.shield_wear == 0){
            if(this.sh1_click){
                cc.find('Canvas/equipment/normal_shield_btn/choose').getComponent(cc.Label).string = "";
                let database = firebase.database().ref('man/bag');
                database.update({
                    equipshield: 1, 
                });
                let def_num = cc.find('Canvas/UI/def/def_num').getComponent(cc.Label).string;
                let defnum;
                defnum = parseInt(def_num);
                database = firebase.database().ref('man');
                database.update({
                    def: defnum + 10
                })
                this.shield_wear = 1;
                this.sh1_click = false;
            }
            else if(this.sw1_click){
                cc.find('Canvas/equipment/normal_weapon_btn/choose').getComponent(cc.Label).string = "";
                this.sw1_click = false;
            }
            else if(this.sw2_click){
                cc.find('Canvas/equipment/ex_sword_btn/choose').getComponent(cc.Label).string = "";
                this.sw2_click = false;
            }
            else if(this.sh2_click){
                cc.find('Canvas/equipment/ex_shield_btn/choose').getComponent(cc.Label).string = "";
                this.sh2_click = false;
            }
            if(!this.sh1_click && this.shield_wear == 0){
                this.sh1_click = true;
                cc.find('Canvas/equipment/normal_shield_btn/choose').getComponent(cc.Label).string = '\u9078\u53d6';
            }
        }
        else if(this.shield_wear == 2){
            if(this.sh1_click){
                cc.find('Canvas/equipment/normal_shield_btn/choose').getComponent(cc.Label).string = "";
                let database = firebase.database().ref('man/bag');
                database.update({
                    equipshield: 1, 
                });
                let def_num = cc.find('Canvas/UI/def/def_num').getComponent(cc.Label).string;
                let defnum;
                defnum = parseInt(def_num);
                database = firebase.database().ref('man');
                database.update({
                    def: defnum - 5
                })
                this.shield_wear = 1;
                this.sh1_click = false;
            }
            else if(this.sw1_click){
                cc.find('Canvas/equipment/normal_weapon_btn/choose').getComponent(cc.Label).string = "";
                this.sw1_click = false;
            }
            else if(this.sw2_click){
                cc.find('Canvas/equipment/ex_sword_btn/choose').getComponent(cc.Label).string = "";
                this.sw2_click = false;
            }
            else if(this.sh2_click){
                cc.find('Canvas/equipment/ex_shield_btn/choose').getComponent(cc.Label).string = "";
                this.sh2_click = false;
            }
            if(!this.sh1_click && this.shield_wear == 2){
                this.sh1_click = true;
                cc.find('Canvas/equipment/normal_shield_btn/choose').getComponent(cc.Label).string = '\u9078\u53d6';
            }
        }
    }

    sword2_btn(){
        if(this.sword_wear == 0){
            if(this.sw2_click){
                cc.find('Canvas/equipment/ex_sword_btn/choose').getComponent(cc.Label).string = "";
                let database = firebase.database().ref('man/bag');
                database.update({
                    equipsword: 2, 
                });
                let atk_num = cc.find('Canvas/UI/atk/atk_num').getComponent(cc.Label).string;
                let atknum;
                atknum = parseInt(atk_num);
                database = firebase.database().ref('man');
                database.update({
                    atk: atknum + 50
                })
                this.sword_wear = 2;
                this.sw2_click = false;
            }
            else if(this.sw1_click){
                cc.find('Canvas/equipment/normal_weapon_btn/choose').getComponent(cc.Label).string = "";
                this.sw1_click = false;
            }
            else if(this.sh1_click){
                cc.find('Canvas/equipment/normal_shield_btn/choose').getComponent(cc.Label).string = "";
                this.sh1_click = false;
            }
            else if(this.sh2_click){
                cc.find('Canvas/equipment/ex_shield_btn/choose').getComponent(cc.Label).string = "";
                this.sh2_click = false
            }
            if(!this.sw2_click && this.sword_wear == 0){
                this.sw2_click = true;
                cc.find('Canvas/equipment/ex_sword_btn/choose').getComponent(cc.Label).string = '\u9078\u53d6';
            }
        }
        else if(this.sword_wear == 1){
            if(this.sw2_click){
                cc.find('Canvas/equipment/ex_sword_btn/choose').getComponent(cc.Label).string = "";
                let database = firebase.database().ref('man/bag');
                database.update({
                    equipsword: 2, 
                });
                let atk_num = cc.find('Canvas/UI/atk/atk_num').getComponent(cc.Label).string;
                let atknum;
                atknum = parseInt(atk_num);
                database = firebase.database().ref('man');
                database.update({
                    atk: atknum + 30
                })
                this.sword_wear = 2;
                this.sw2_click = false;
            }
            else if(this.sw1_click){
                cc.find('Canvas/equipment/normal_weapon_btn/choose').getComponent(cc.Label).string = "";
                this.sw1_click = false;
            }
            else if(this.sh1_click){
                cc.find('Canvas/equipment/normal_shield_btn/choose').getComponent(cc.Label).string = "";
                this.sh1_click = false;
            }
            else if(this.sh2_click){
                cc.find('Canvas/equipment/ex_shield_btn/choose').getComponent(cc.Label).string = "";
                this.sh2_click = false;
            }
            if(!this.sw2_click && this.sword_wear == 1){
                this.sw2_click = true;
                cc.find('Canvas/equipment/ex_sword_btn/choose').getComponent(cc.Label).string = '\u9078\u53d6';
            }
        }
    }

    shield2_btn(){
        if(this.shield_wear == 0){
            if(this.sh2_click){
                cc.find('Canvas/equipment/ex_shield_btn/choose').getComponent(cc.Label).string = "";
                let database = firebase.database().ref('man/bag');
                database.update({
                    equipshield: 2, 
                });
                let def_num = cc.find('Canvas/UI/def/def_num').getComponent(cc.Label).string;
                let defnum;
                defnum = parseInt(def_num);
                database = firebase.database().ref('man');
                database.update({
                    def: defnum + 15
                })
                this.shield_wear = 2;
                this.sh2_click = false;
            }
            else if(this.sw1_click){
                cc.find('Canvas/equipment/normal_weapon_btn/choose').getComponent(cc.Label).string = "";
                this.sw1_click = false;
            }
            else if(this.sh1_click){
                cc.find('Canvas/equipment/normal_shield_btn/choose').getComponent(cc.Label).string = "";
                this.sh1_click = false;
            }
            else if(this.sw2_click){
                cc.find('Canvas/equipment/ex_sword_btn/choose').getComponent(cc.Label).string = "";
                this.sw2_click = false;
            }
            if(!this.sh2_click && this.shield_wear == 0){
                this.sh2_click = true;
                cc.find('Canvas/equipment/ex_shield_btn/choose').getComponent(cc.Label).string = '\u9078\u53d6';
            }
        }
        else if(this.shield_wear == 1){
            if(this.sh2_click){
                cc.find('Canvas/equipment/ex_shield_btn/choose').getComponent(cc.Label).string = "";
                let database = firebase.database().ref('man/bag');
                database.update({
                    equipshield: 2, 
                });
                let def_num = cc.find('Canvas/UI/def/def_num').getComponent(cc.Label).string;
                let defnum;
                defnum = parseInt(def_num);
                database = firebase.database().ref('man');
                database.update({
                    def: defnum + 5
                })
                this.shield_wear = 2;
                this.sh2_click = false;
            }
            else if(this.sw1_click){
                cc.find('Canvas/equipment/normal_weapon_btn/choose').getComponent(cc.Label).string = "";
                this.sw1_click = false;
            }
            else if(this.sh1_click){
                cc.find('Canvas/equipment/normal_shield_btn/choose').getComponent(cc.Label).string = "";
                this.sh1_click = false;
            }
            else if(this.sw2_click){
                cc.find('Canvas/equipment/ex_sword_btn/choose').getComponent(cc.Label).string = "";
                this.sw2_click = false;
            }
            if(!this.sh2_click && this.shield_wear == 1){
                this.sh2_click = true;
                cc.find('Canvas/equipment/ex_shield_btn/choose').getComponent(cc.Label).string = '\u9078\u53d6';
            }
        }
    }

    async onLoad () {
        let sword1: number = 0;
        let sword2: number = 0;
        let shield1: number = 0;
        let shield2: number = 0;
        let sword_equip: number = 0;
        let shield_equip: number = 0;
        let database = firebase.database().ref('man');
        await database.once('value').then(function(snapshot){
            snapshot.forEach(function(childShot){
                if(childShot.key == 'bag'){
                    let childData = childShot.val();
                    sword1 = childData.sword1;
                    sword2 = childData.sword2;
                    shield1 = childData.shield1;
                    shield2 = childData.shield2;
                    sword_equip = childData.equipsword;
                    shield_equip = childData.equipshield;
                }
            })
        })
        
        this.normal_sword_get = sword1;
        this.normal_shield_get = shield1;
        this.ex_sword_get = sword2;
        this.ex_shield_get = shield2;
        this.sword_wear = sword_equip;
        this.shield_wear = shield_equip;

        if(this.normal_sword_get != 0){
            let ns = cc.instantiate(this.normal_sword_prefab)

            this.node.addChild(ns);
            ns.setPosition(cc.v2(this.equip_button_x, this.equip_button_y));
            this.equip_button_x += 125;

            let button_event = new cc.Component.EventHandler();
            button_event.target = this.node;
            button_event.component = "equipment";
            button_event.handler = "sword1_btn";
            ns.getComponent(cc.Button).clickEvents.push(button_event);
        }
        if(this.normal_shield_get != 0){
            let ns = cc.instantiate(this.normal_shield_prefab);
            this.node.addChild(ns);
            ns.setPosition(cc.v2(this.equip_button_x, this.equip_button_y));
            this.equip_button_x += 125;

            let button_event = new cc.Component.EventHandler();
            button_event.target = this.node;
            button_event.component = "equipment";
            button_event.handler = "shield1_btn";
            ns.getComponent(cc.Button).clickEvents.push(button_event);
        }
        if(this.ex_sword_get != 0){
            let es = cc.instantiate(this.ex_sword_prefab);
            this.node.addChild(es);
            es.setPosition(cc.v2(this.equip_button_x, this.equip_button_y));
            this.equip_button_x += 125;

            let button_event = new cc.Component.EventHandler();
            button_event.target = this.node;
            button_event.component = "equipment";
            button_event.handler = "sword2_btn";
            es.getComponent(cc.Button).clickEvents.push(button_event);
        }
        if(this.ex_shield_get != 0){
            let es = cc.instantiate(this.ex_shield_prefab);
            this.node.addChild(es);
            es.setPosition(cc.v2(this.equip_button_x, this.equip_button_y));
            this.equip_button_x += 125;

            let button_event = new cc.Component.EventHandler();
            button_event.target = this.node;
            button_event.component = "equipment";
            button_event.handler = "shield2_btn";
            es.getComponent(cc.Button).clickEvents.push(button_event);
        }
    }

    start () {

    }

    sword_display(){
        if(this.sword_wear == 0){
            cc.find('Canvas/equipment/weapon/Background').color = cc.Color.GRAY;
            cc.find('Canvas/equipment/weapon/Background').getComponent(cc.Sprite).spriteFrame = this.df_btn_frame;
            cc.find('Canvas/equipment/weapon/info').getComponent(cc.Label).string = 'ATK + 0';
        }
        else if(this.sword_wear == 1){
            cc.find('Canvas/equipment/weapon/Background').color = cc.Color.WHITE;
            cc.find('Canvas/equipment/weapon/Background').getComponent(cc.Sprite).spriteFrame = this.normal_sword_frame;
            cc.find('Canvas/equipment/weapon/info').getComponent(cc.Label).string = 'ATK + 20';
        }
        else if(this.sword_wear == 2){
            cc.find('Canvas/equipment/weapon/Background').color = cc.Color.WHITE;
            cc.find('Canvas/equipment/weapon/Background').getComponent(cc.Sprite).spriteFrame = this.ex_sword_frame;
            cc.find('Canvas/equipment/weapon/info').getComponent(cc.Label).string = 'ATK + 50';
        }
    }

    shield_display(){
        if(this.shield_wear == 0){
            cc.find('Canvas/equipment/shield/Background').color = cc.Color.GRAY;
            cc.find('Canvas/equipment/shield/Background').getComponent(cc.Sprite).spriteFrame = this.df_btn_frame;
            cc.find('Canvas/equipment/shield/info').getComponent(cc.Label).string = 'DEF + 0';
        }
        else if(this.shield_wear == 1){
            cc.find('Canvas/equipment/shield/Background').color = cc.Color.WHITE;
            cc.find('Canvas/equipment/shield/Background').getComponent(cc.Sprite).spriteFrame = this.normal_shield_frame;
            cc.find('Canvas/equipment/shield/info').getComponent(cc.Label).string = 'DEF + 10';
        }
        else if(this.shield_wear == 2){
            cc.find('Canvas/equipment/shield/Background').color = cc.Color.WHITE;
            cc.find('Canvas/equipment/shield/Background').getComponent(cc.Sprite).spriteFrame = this.ex_shield_frame;
            cc.find('Canvas/equipment/shield/info').getComponent(cc.Label).string = 'DEF + 15';
        }
    }

    update (dt) {
        this.sword_display();
        this.shield_display();
    }
}
