const { ccclass, property } = cc._decorator;
@ccclass
export default class items extends cc.Component {

    @property
    mouse_enble = false;

    @property
    enemy_num = 1; //slime

    @property(cc.Node)
    player: cc.Node = null; // 玩家

    // mouse click for debug
    onload() {
        cc.director.getCollisionManager().enabled = true;
cc.director.getCollisionManager().enabledDrawBoundingBox=true;
        cc.director.getPhysicsManager().enabled = true;
        var body=this.getComponent(cc.RigidBody);
        body.enabledContactListener=true;
    }
    /*onBeginContact(contact, self, other) {
        if (other.node.name == "player") {
            //this.node.stopAllActions();
            //cc.director.loadScene("fight");
            cc.director.runSceneImmediate("fight");
        }
    }*/
    onCollisionEnter(other, self) {
        console.log("other.name = ", other.node.name, other.node.group, other.node.groupIndex);
        if (other.node.name == "player") {
            
            cc.director.loadScene("fight");
        }
    }
}