const { ccclass, property } = cc._decorator;
@ccclass
export default class items extends cc.Component {

    @property
    mouse_enble = false;

    @property
    enemy_num = 4; // knight

    @property(cc.Node)
    player: cc.Node = null; // 玩家

    // mouse click for debug
    start () {
        this.node.on('mousedown', function(event){
            if(this.mouse_enble == false)
                return;
            console.log('Get a normal shield!');
            // 消失
            this.node.removeFromParent();
            // 放到背包

        }, this);
    }


    onCollisionEnter(other, self) {
        console.log("other.name = ", other.node.name, other.node.group, other.node.groupIndex);
        if (other.node.groupIndex === 1) { // 表示是PLAYER类型撞到了，道具拾取成功
            // 消失
            this.node.removeFromParent();
            // 放到背包

        }
    }

    // 裝備
    equip_normal_shield () {
        let defense_inc = 100;
        // this.player.defense += defense_inc;
    }
}