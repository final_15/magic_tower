const { ccclass, property } = cc._decorator;
@ccclass
export default class items extends cc.Component {

    @property
    mouse_enble = false;

    @property
    enemy_num = 1; //slime

    @property(cc.Node)
    player: cc.Node = null; // 玩家



    // mouse click for debug
    onload() {
        cc.director.getCollisionManager().enabled = true;

        cc.director.getPhysicsManager().enabled = true;
    }
    onCollisionEnter(other, self) {
        console.log("other.name = ", other.node.name, other.node.group, other.node.groupIndex);
        this.change_type(self);
        if (other.node.name == "player") {
            cc.log("1234567");
            cc.director.loadScene("fight");
        }
    }
    change_type(self) {
        var ref = firebase.database().ref('fight/type');
        if (self.tag == 22) ref.set(1);
        else if (self.tag == 24) ref.set(2);
        else if (self.tag == 23) ref.set(3);
        else if (self.tag == 25) ref.set(4);
        else if (self.tag == 26) ref.set(5);
        var ref = firebase.database().ref('fight/place');
        ref.set(self.node.name)
    }
}