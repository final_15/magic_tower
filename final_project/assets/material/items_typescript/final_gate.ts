const {ccclass, property} = cc._decorator;
declare var firebase: any;

@ccclass
export default class gates extends cc.Component {


    @property(cc.AudioClip)
    open_effect:cc.AudioClip = null;

    private anim:cc.Animation = null;

    // stage flag
    private stage = 0;

    async onLoad(){
        this.anim = this.node.getComponent(cc.Animation);
        var ref = firebase.database().ref('nowscene');
        await ref.once('value').then((snap)=>{
            this.stage = snap.val().stage;
        });
    }

    start(){

    }

    update(){

    }

    onBeginContact(contact, self, other) {
        cc.log(other.node.x)
        cc.log(other.node.y)
        if (other.tag == 1) {
            this.open_final_gate(this.node, self);
        }
    }

    // 開門
    async open_final_gate (gate, self) {
        var can_open = false;
        var ref = firebase.database().ref('man/bag/finalkey');
        await ref.once('value').then((snap)=>{
            if(snap.val() > 0) {
                ref.set(snap.val() - 1);
                can_open = true;
            } else {
                cc.log("You don't have any final key.")
            }
        });
        if(can_open) {
            cc.audioEngine.playEffect(this.open_effect,false);
            let data = firebase.database().ref(this.stage.toString() + '/gate/' + self.node.name);
            data.set(0)
            // play animation
            this.anim.play('final_gate');
            // remove gate
            this.scheduleOnce(function(){
                this.node.destroy();
            }, 0.35);
        }
    }
}
