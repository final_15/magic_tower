const { ccclass, property } = cc._decorator;

@ccclass
export default class items extends cc.Component {

    @property
    mouse_enble = false;

    @property
    potion_num = 2; // HP 中

    @property
    flag = "a";

    @property
    scene = 1;

    // mouse click for debug
    start () {
        this.node.on('mousedown', function(event){
            if(this.mouse_enble == false)
                return;
            console.log('Get a medium HP potion!');
            // 消失
            this.node.removeFromParent();
            // 放到背包
            this.put_into_bag();
            // Change flag
            this.change_flag();
        }, this);
    }

    onCollisionEnter(other, self) {
        console.log("other.name = ", other.node.name, other.node.group, other.node.groupIndex);
        if (other.tag === 1) { // 表示是PLAYER类型撞到了，道具拾取成功
            // 消失
            this.node.removeFromParent();
            // 放到背包
            this.put_into_bag();
            // Change flag
            this.change_flag();
        }
    }

    // 在背包或戰鬥中使用
    hp_increase () {
        let hp_inc = 200;
        var ref = firebase.database().ref('man/hp');
        ref.once('value').then((snap)=>{
            ref.set(snap.val() + hp_inc);
        });
    }

    // 放到背包
    put_into_bag() {
        var ref = firebase.database().ref('man/bag/bloodmid');
        ref.once('value').then((snap)=>{
            ref.set(snap.val() + 1);
        });
    }

    // flag
    change_flag () {
        var path = this.scene.toString();
        path = path.concat("/item/");
        path = path.concat(this.flag);

        console.log("Flag path is", path);

        var ref = firebase.database().ref(path);
        ref.once('value').then((snap)=>{
            ref.set(0);
        });
    }
}