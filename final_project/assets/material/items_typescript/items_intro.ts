const {ccclass, property} = cc._decorator;

@ccclass
export default class items extends cc.Component {

    @property
    special_num = 1; // 道具介紹書

    @property(cc.Prefab)
    intro: cc.Prefab = null; // 介紹

    @property
    flag = true; // 可觀看的情況

    onLoad () {
        cc.systemEvent.on(cc.SystemEvent.EventType.KEY_DOWN, this.onKeyDown, this);
    }

    onDestroy () {
        cc.systemEvent.off(cc.SystemEvent.EventType.KEY_DOWN, this.onKeyDown, this);
    }

    onKeyDown (event) {
        switch(event.keyCode) {
            case cc.macro.KEY.enter:
                console.log('Open items intro.');
                if(this.flag) {
                    this.watch_item_intro();
                }
                break;
        }
    }
    

    // 觀看介紹
    watch_item_intro () {
        if(cc.isValid(cc.find('Canvas/Items Intro'))){
            cc.find('Canvas/Items Intro').destroy();
        }
        else{
            let intro_window = cc.instantiate(this.intro);
            cc.find('Canvas').addChild(intro_window);
        }
    }
}
