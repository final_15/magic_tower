const {ccclass, property} = cc._decorator;

@ccclass
export default class items extends cc.Component {

    @property
    is_enable = true;

    @property
    is_debug = true;

    // use this for initialization
    onLoad () {
        
    }

    start () {
        if(this.is_enable == true) {
            var manager = cc.director.getCollisionManager(); // 获取碰撞管理器
            manager.enabled = true; // 开启碰撞
            manager.enabledDebugDraw = false; // 允许绘制碰撞的区域
        }
    }

    // called every frame
    upload (dt) {
        
    }
}