const { ccclass, property } = cc._decorator;

@ccclass
export default class items extends cc.Component {

    @property
    mouse_enble = false;

    @property
    potion_num = 6; // 攻擊水晶

    @property
    flag = "a";

    @property
    scene = 1;

    // mouse click for debug
    start () {
        this.node.on('mousedown', function(event){
            if(this.mouse_enble == false)
                return;
            console.log('Get an attack crystal!');
            // 消失
            this.node.removeFromParent();
            // 直接使用
            this.attack_increase();
            // Change flag
            this.change_flag();
        }, this);
    }

    onCollisionEnter(other, self) {
        console.log("other.name = ", other.node.name, other.node.group, other.node.groupIndex);
        if (other.tag === 1) { // 表示是PLAYER类型撞到了，道具拾取成功
            // 消失
            this.node.removeFromParent();
            // 直接使用
            this.attack_increase();
            // Change flag
            this.change_flag();
        }
    }

    // 在背包或戰鬥中使用
    attack_increase () {
        let attack_inc = 3;
        var ref = firebase.database().ref('man/atk');
        ref.once('value').then((snap)=>{
            ref.set(snap.val() + attack_inc);
        });
    }

    // flag
    change_flag () {
        var path = this.scene.toString();
        path = path.concat("/item/");
        path = path.concat(this.flag);

        console.log("Flag path is", path);

        var ref = firebase.database().ref(path);
        ref.once('value').then((snap)=>{
            ref.set(0);
        });
    }
}