const { ccclass, property } = cc._decorator;

@ccclass
export default class items extends cc.Component {

    @property
    mouse_enble = false;

    @property
    potion_num = 5; // 復活

    @property
    flag = "a";

    @property
    scene = 1;

    // mouse click for debug
    start () {
        this.node.on('mousedown', function(event){
            if(this.mouse_enble == false)
                return;
            console.log('Get a revive potion!');
            // 消失
            this.node.removeFromParent();
            // 放到背包
            this.put_into_bag();
            // Change flag
            this.change_flag();
        }, this);
    }

    onCollisionEnter(other, self) {
        console.log("other.name = ", other.node.name, other.node.group, other.node.groupIndex);
        if (other.tag === 1) { // 表示是PLAYER类型撞到了，道具拾取成功
            // 消失
            this.node.removeFromParent();
            // 放到背包
            this.put_into_bag();
            // Change flag
            this.change_flag();
        }
    }

    // 戰鬥中使用
    revive () {
        // 補滿
        let hp_inc = 500;
        var ref = firebase.database().ref('man/hp');
        ref.once('value').then((snap)=>{
            ref.set(hp_inc);
        });
        // 繼續遊戲
    }

    // 放到背包
    put_into_bag() {
        var ref = firebase.database().ref('man/bag/revive');
        ref.once('value').then((snap)=>{
            ref.set(1);
        });
    }

    // flag
    change_flag () {
        var path = this.scene.toString();
        path = path.concat("/item/");
        path = path.concat(this.flag);

        console.log("Flag path is", path);

        var ref = firebase.database().ref(path);
        ref.once('value').then((snap)=>{
            ref.set(0);
        });
    }
}