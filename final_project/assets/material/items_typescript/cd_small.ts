const { ccclass, property } = cc._decorator;

@ccclass
export default class items extends cc.Component {

    @property
    mouse_enble = false;

    @property
    potion_num = 4; // CD藥水

    @property
    flag = "a";

    @property
    scene = 1;

    // mouse click for debug
    start() {
        this.node.on('mousedown', function (event) {
            if (this.mouse_enble == false)
                return;
            console.log('Get a MP potion!');
            // 消失
            this.node.removeFromParent();
            // 放到背包
            this.put_into_bag();
            // Change flag
            this.change_flag();
        }, this);
    }

    onCollisionEnter(other, self) {
        console.log("other.name = ", other.node.name, other.node.group, other.node.groupIndex);
        if (other.tag === 1) { // 表示是PLAYER类型撞到了，道具拾取成功
            // 消失
            this.node.removeFromParent();
            // 放到背包
            this.put_into_bag();
            // Change flag
            this.change_flag();
        }
    }

    // 在背包或戰鬥中使用
    cd_decrease() {
        var cd_num = this.getComponent('fight_ts');
        if (cd_num.cd_a_num > 0) {
            cd_num.cd_a_num -= 1;
        }
        if (cd_num.cd_b_num > 0) {
            cd_num.cd_b_num -= 1;
        }
        if (cd_num.cd_c_num > 0) {
            cd_num.cd_c_num -= 1;
        }
    }

    // 放到背包
    put_into_bag() {
        var ref = firebase.database().ref('man/bag/cd');
        ref.once('value').then((snap) => {
            ref.set(snap.val() + 1);
        });
    }

    // flag
    change_flag () {
        var path = this.scene.toString();
        path = path.concat("/item/");
        path = path.concat(this.flag);

        console.log("Flag path is", path);

        var ref = firebase.database().ref(path);
        ref.once('value').then((snap)=>{
            ref.set(0);
        });
    }
}