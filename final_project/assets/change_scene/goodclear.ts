
const {ccclass, property} = cc._decorator;

@ccclass
export default class goodclear extends cc.Component {

    @property(cc.AudioClip)
    effect: cc.AudioClip = null;

    start(){
        cc.audioEngine.playMusic(this.effect, false)
        this.scheduleOnce(()=>{
            cc.director.loadScene("Menu");
        },3);
    }
}
