const {ccclass, property} = cc._decorator;
declare var firebase: any;

@ccclass
export default class Start extends cc.Component {

    start(){
        this.reset_data();
        this.scheduleOnce(()=>{
            cc.director.loadScene("1");
        },5);
    }

    reset_data(){
        
        var ref1 = firebase.database().ref('man');
        ref1.once('value').then(((snap)=>{
            var data = {
                hp : 500,
                atk: 50,
                def: 10,
                posX: 0,
                posY: 0,
                revive: 0,
            }
            ref1.set(data);
            var ref2 = firebase.database().ref('man/skill');
            ref2.once('value').then(((snap)=>{
                var data = {
                    a: 0,
                    b: 0,
                    c: 0,
                }
                ref2.set(data);
            }));
            var ref3 = firebase.database().ref('man/bag');
            ref3.once('value').then(((snap)=>{
                var data = {
                    equipsword : 0,
                    equipshield: 0,
                    bloodsmall: 0,
                    bloodmid: 0,
                    bloodbig: 0,
                    cd: 0,
                    revive: 0,
                    sword1: 0,
                    sword2: 0,
                    shield1: 0,
                    shield2: 0,
                    coin: 0,
                    key: 0,
                    exkey: 0,
                    finalkey: 0,
                }
                ref3.set(data);
            }));
            
        }));

        var ref4 = firebase.database().ref('nowscene');
        ref4.once('value').then(((snap)=>{
            var data = {
                stage : 1,
            }
            ref4.set(data);

        }));
        var ref5 = firebase.database().ref('fight');
        ref5.once('value').then(((snap)=>{
            var data = {
                type: 0,
                place: 'a',
                testhp: 10,
                testdef: 10,
                testatk: 10,
            }
            ref5.set(data);
        }));
        var ref11 = firebase.database().ref('1/monster');
        ref11.once('value').then(((snap)=>{
            var data = {
                a: 1,
                b: 1,
                c: 1,
                d: 1,
                e: 1,
            }
            ref11.set(data);
        }));
        var ref12 = firebase.database().ref('1/item');
        ref12.once('value').then(((snap)=>{
            var data = {
                a: 1,
                b: 1,
                c: 1,
                d: 1,
                e: 1,
                f: 1,
                g: 1,
                h: 1,
                i: 1,
                j: 1,

            }
            ref12.set(data);
        }));
        var ref13 = firebase.database().ref('1/gate');
        ref13.once('value').then(((snap)=>{
            var data = {
                a: 1,
                b: 1,
                c: 1,
                d: 1,
                e: 1,
            }
            ref13.set(data);
        }));
        var ref14 = firebase.database().ref('1/pos');
        ref14.once('value').then(((snap)=>{
            var data = {
                x: -176,
                y: 272,
                enemy_pos: 0,
                prev_stage: '',
            }
            ref14.set(data);
        }));
        var ref21 = firebase.database().ref('2/monster');
        ref21.once('value').then(((snap)=>{
            var data = {
                a: 1,
                b: 1,
                c: 1,
                d: 1,
                e: 1,
            }
            ref21.set(data);
        }));
        var ref22 = firebase.database().ref('2/item');
        ref22.once('value').then(((snap)=>{
            var data = {
                a: 1,
                b: 1,
                c: 1,
                d: 1,
                e: 1,
                f: 1,
                g: 1,
                h: 1,
                i: 1,
                j: 1,

            }
            ref22.set(data);
        }));
        var ref23 = firebase.database().ref('2/gate');
        ref23.once('value').then(((snap)=>{
            var data = {
                a: 1,
                b: 1,
                c: 1,
                d: 1,
                e: 1,
            }
            ref23.set(data);
        }));
        var ref24 = firebase.database().ref('2/pos');
        ref24.once('value').then(((snap)=>{
            var data = {
                x: 432,
                y: 272,
                enemy_pos: 0,
                prev_stage: '',
            }
            ref24.set(data);
        }));
        var ref31 = firebase.database().ref('3/monster');
        ref31.once('value').then(((snap)=>{
            var data = {
                a: 1,
                b: 1,
                c: 1,
                d: 1,
                e: 1,
            }
            ref31.set(data);
        }));
        var ref32 = firebase.database().ref('3/item');
        ref32.once('value').then(((snap)=>{
            var data = {
                a: 1,
                b: 1,
                c: 1,
                d: 1,
                e: 1,
                f: 1,
                g: 1,
                h: 1,
                i: 1,
                j: 1,

            }
            ref32.set(data);
        }));
        var ref33 = firebase.database().ref('3/gate');
        ref33.once('value').then(((snap)=>{
            var data = {
                a: 1,
                b: 1,
                c: 1,
                d: 1,
                e: 1,
            }
            ref33.set(data);
        }));
        var ref34 = firebase.database().ref('3/pos');
        ref34.once('value').then(((snap)=>{
            var data = {
                x: 112,
                y: 208,
                enemy_pos: 0,
                prev_stage: '',
            }
            ref34.set(data);
        }));
        var ref41 = firebase.database().ref('4/monster');
        ref41.once('value').then(((snap)=>{
            var data = {
                a: 1,
                b: 1,
                c: 1,
                d: 1,
                e: 1,
            }
            ref41.set(data);
        }));
        var ref42 = firebase.database().ref('4/item');
        ref42.once('value').then(((snap)=>{
            var data = {
                a: 1,
                b: 1,
                c: 1,
                d: 1,
                e: 1,
                f: 1,
                g: 1,
                h: 1,
                i: 1,
                j: 1,

            }
            ref42.set(data);
        }));
        var ref43 = firebase.database().ref('4/gate');
        ref43.once('value').then(((snap)=>{
            var data = {
                a: 1,
                b: 1,
                c: 1,
                d: 1,
                e: 1,
            }
            ref43.set(data);
        }));
        var ref44 = firebase.database().ref('4/pos');
        ref44.once('value').then(((snap)=>{
            var data = {
                x: 176,
                y: -272,
                enemy_pos: 0,
                prev_stage: '',
            }
            ref44.set(data);
        }));
        var ref51 = firebase.database().ref('5/monster');
        ref51.once('value').then(((snap)=>{
            var data = {
                a: 1,
                b: 1,
                c: 1,
                d: 1,
                e: 1,
            }
            ref51.set(data);
        }));
        var ref52 = firebase.database().ref('5/item');
        ref52.once('value').then(((snap)=>{
            var data = {
                a: 1,
                b: 1,
                c: 1,
                d: 1,
                e: 1,
                f: 1,
                g: 1,
                h: 1,
                i: 1,
                j: 1,

            }
            ref52.set(data);
        }));
        var ref53 = firebase.database().ref('5/gate');
        ref53.once('value').then(((snap)=>{
            var data = {
                a: 1,
                b: 1,
                c: 1,
                d: 1,
                e: 1,
            }
            ref53.set(data);
        }));
        var ref54 = firebase.database().ref('5/pos');
        ref54.once('value').then(((snap)=>{
            var data = {
                x: 112,
                y: -240,
            }
            ref54.set(data);
        }));
        var ref61 = firebase.database().ref('6/gate');
        ref61.once('value').then(((snap)=>{
            var data = {
                a: 1,
                b: 1,
            }
            ref61.set(data);
        }));
        var ref64 = firebase.database().ref('6/pos');
        ref64.once('value').then(((snap)=>{
            var data = {
                x: 112,
                y: -272,
                enemy_pos: 0,
                prev_stage: '',
            }
            ref64.set(data);
        }));
        
    }

}
