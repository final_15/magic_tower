
const {ccclass, property} = cc._decorator;

@ccclass
export default class badclear extends cc.Component {


    @property(cc.Node)
    label1:cc.Node = null;
    @property(cc.Node)
    label2:cc.Node = null;

    @property(cc.Node)
    princess:cc.Node = null;
    @property(cc.SpriteFrame)
    afterprincess:cc.SpriteFrame = null;
    @property(cc.Node)
    cross:cc.Node = null;

    @property(cc.AudioClip)
    effect: cc.AudioClip = null;


    start(){
        this.scheduleOnce(()=>{
            cc.director.loadScene("Menu");
        },6);
        this.scheduleOnce(()=>{
            this.change();
        },3);
    }
    change(){
        this.princess.getComponent(cc.Sprite).spriteFrame = this.afterprincess;
        this.label2.getComponent(cc.Label).string = 'Clear??';
        this.label1.color = cc.color(255, 0 , 0);
        this.label2.color = cc.color(255, 0 , 0);
        this.cross.x = -50;
        this.cross.y = 40;
        cc.audioEngine.playEffect(this.effect, false); 
    }
}
