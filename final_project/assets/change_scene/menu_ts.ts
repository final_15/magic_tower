const {ccclass, property} = cc._decorator;

@ccclass
export default class Menu extends cc.Component {

    @property(cc.AudioClip)
    bgm: cc.AudioClip = null;

    changscene(){
        cc.audioEngine.stopMusic();
        cc.director.loadScene("Start");
    }
    onLoad(){
        cc.audioEngine.playMusic(this.bgm, true);
    }   

}
